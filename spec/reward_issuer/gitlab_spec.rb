# frozen_string_literal: true

require 'gitlab'

require_relative '../../lib/reward_issuer/gitlab'

RSpec.describe RewardIssuer::Gitlab do
  subject(:issuer) { described_class.new }

  before do
    stub_env('GITLAB_ENDPOINT', 'endpoint')
    stub_env('GITLAB_TOKEN', 'gl-token')

    allow(Gitlab).to receive(:client)
  end

  describe '.validate_args!' do
    let(:args) { { username: 'lee' } }

    it 'does not raise an error' do
      expect { issuer.validate_args!(args) }.not_to raise_error(ArgumentError)
    end

    describe 'when the username argument is missing' do
      let(:args) { {} }

      it 'raises an argument error' do
        expect { issuer.validate_args!(args) }.to raise_error(ArgumentError, 'Username cannot be nil')
      end
    end
  end
end
