# frozen_string_literal: true

require_relative '../../lib/reward_issuer/base'

RSpec.describe RewardIssuer::Base do
  describe '.issue' do
    it 'raises a NotImplementedError' do
      expect { described_class.new.issue(1) }.to raise_error(NotImplementedError)
    end
  end

  describe '.validate_args!' do
    it 'raises a NotImplementedError' do
      expect { described_class.new.validate_args!(1) }.to raise_error(NotImplementedError)
    end
  end
end
