# frozen_string_literal: true

require_relative '../../lib/helper/message_template'
require_relative '../../lib/reward_issuer/base'
require_relative '../../lib/templates/base'

RSpec.describe Helper::MessageTemplate do
  let(:reward_issuer) { instance_double(RewardIssuer::Base) }
  let(:template) { instance_double(Templates::Base) }
  let(:args) { {} }

  subject(:helper) { described_class.new(reward_issuer, template) }

  before do
    allow(reward_issuer).to receive(:validate_args!).with(args)
    allow(template).to receive(:validate_args!).with(args)
  end

  describe '.build' do
    before do
      allow(template).to receive(:build).with(args)
    end

    it 'calls the underlying template.build' do
      helper.build(args)

      expect(template).to have_received(:build).with(args)
    end
  end

  describe '.validate_args!' do
    subject(:validate_args!) { helper.validate_args!(args) }

    context 'when the args are a hash' do
      it 'validates args against the reward issuer issuer and the template' do
        validate_args!

        expect(reward_issuer).to have_received(:validate_args!).with(args)
        expect(template).to have_received(:validate_args!).with(args)
      end
    end

    context 'when the args are not a hash' do
      let(:args) { 'string' }

      it 'raises an argument error' do
        expect { validate_args! }.to raise_error(ArgumentError, 'Message cannot be nil')
      end
    end
  end
end
