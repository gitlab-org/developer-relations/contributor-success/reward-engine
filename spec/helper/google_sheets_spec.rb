# frozen_string_literal: true

require_relative '../../lib/helper/google_sheets'

RSpec.describe Helper::GoogleSheets do
  subject(:helper) { described_class.new(sheet_id, 'MySheet') }

  let(:sheet_service) { instance_double(Google::Apis::SheetsV4::SheetsService) }
  let(:sheet_id) { '13hO68dfbZeS63pbK4Q30503NGpGi5aD1PFrqsXSFgfY' }

  before do
    stub_env('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH', File.expand_path('../gsa.json', __dir__))
    allow(Google::Apis::SheetsV4::SheetsService).to receive(:new).and_return(sheet_service)
    allow(sheet_service).to receive(:authorization=)
    allow(sheet_service).to receive(:get_spreadsheet_values).and_return(
      Google::Apis::SheetsV4::ValueRange.new(
        range: "MySheet!A1:Z1000",
        majorDimension: "ROWS",
        values:
          [
            ["Username", "Code", "Sent on"],
            ["leetickett-gitlab", "abc123", "2023-11-14"],
            ["stingrayza", "a8b87987-75e4-43f1-8f54-43eea398bfc9"]
          ]
      )
    )
  end

  describe '#read' do
    it 'reads the sheet' do
      result = helper.read

      expect(result.count).to be(2)
      expect(result).to include(
        {
          'Code' => 'abc123',
          'Sent on' => '2023-11-14',
          'Username' => 'leetickett-gitlab',
          :row_index => 2
        }
      )
    end
  end

  describe '#get_header_column' do
    it 'returns the sheet column of the header field' do
      result = helper.get_header_column('Code')

      expect(result).to eq('B')
    end
  end

  describe '#calculate_sheet_range' do
    it 'returns the range of the sheet' do
      result = helper.calculate_sheet_range(
        {
          'Code' => 'abc123',
          'Sent on' => '2023-11-14',
          'Username' => 'leetickett-gitlab',
          :row_index => 4
        }
      )

      expect(result).to eq('A4:C4')
    end
  end

  describe '#write' do
    let(:range) { 'MySheet!B3' }
    let(:cell_count) { 1 }
    let(:update_response) do
      Google::Apis::SheetsV4::UpdateValuesResponse.new(
        updated_cells: cell_count
      )
    end

    before do
      allow(sheet_service).to receive(:update_spreadsheet_value).and_return(update_response)
    end

    context 'when a single cell' do
      it 'writes to the sheet' do
        expect(helper.write('B3', 'abc123')).to eq(1)
        expect(sheet_service).to have_received(:update_spreadsheet_value).with(
          sheet_id,
          range,
          have_attributes(values: [['abc123']]),
          value_input_option: 'USER_ENTERED'
        )
      end
    end

    context 'when a full row' do
      let(:range) { 'MySheet!C1:C3' }
      let(:cell_count) { 3 }

      it 'writes to the sheet' do
        expect(helper.write('C1:C3', %w[abc123 def456 ghi789])).to eq(3)
        expect(sheet_service).to have_received(:update_spreadsheet_value).with(
          sheet_id,
          range,
          have_attributes(values: [%w[abc123 def456 ghi789]]),
          value_input_option: 'USER_ENTERED'
        )
      end
    end
  end

  describe '#write_values' do
    let(:row) { '4' }
    let(:values) do
      {
        'Sent on' => '2023-11-14',
        'Username' => 'leetickett-gitlab'
      }
    end

    let(:batch_update_response) do
      Google::Apis::SheetsV4::BatchUpdateValuesResponse.new(
        total_updated_cells: 2
      )
    end

    before do
      allow(sheet_service).to receive(:batch_update_values).and_return(batch_update_response)
    end

    it 'writes multiple values to the sheet' do
      expect(helper.write_values(row, values)).to eq(2)
      expect(sheet_service).to have_received(:batch_update_values).with(
        sheet_id,
        have_attributes(
          data: contain_exactly(
            have_attributes(
              range: "MySheet!A#{row}",
              values: [['leetickett-gitlab']]
            ),
            have_attributes(
              range: "MySheet!C#{row}",
              values: [['2023-11-14']]
            )
          ),
          value_input_option: 'USER_ENTERED'
        )
      )
    end
  end
end
