# frozen_string_literal: true

require_relative '../../lib/reward_provider/boundless'

RSpec.describe RewardProvider::Boundless do
  let(:instance) { described_class.new }
  let(:helper) { instance_double(Helper::GoogleSheets) }
  let(:test_sheet) do
    [{ "Items or value" => "$10.00",
       "Code" => "SMALL_CODE",
       "Ready to disperse? Ask Fatima or Nick if no" => "Yes",
       "Email Recipient" => "robin@example.com",
       "Prize" => "First MR Merged",
       "Event Results" => "",
       "Email sent?" => "Yes",
       "Redeemed" => "No",
       "Tracking " => nil,
       "Sent by" => nil,
       "Sent On" => nil,
       "Duplicate?" => nil,
       "Notes" => nil,
       :row_index => 2 },
      { "Items or value" => "$25.00",
        "Code" => "USED_CODE",
        "Ready to disperse? Ask Fatima or Nick if no" => "Yes",
        "Email Recipient" => "alex@example.com",
        "Prize" => "Hackathon Prize",
        "Event Results" => "https://example.com",
        "Email sent?" => "Yes",
        "Redeemed" => "No",
        "Tracking " => nil,
        "Sent by" => nil,
        "Sent On" => nil,
        "Duplicate?" => nil,
        "Notes" => nil,
        :row_index => 3 },
      { "Items or value" => "$25.00",
        "Code" => "AVAILABLE_CODE",
        "Ready to disperse? Ask Fatima or Nick if no" => "Yes",
        "Email Recipient" => "",
        "Prize" => "",
        "Event Results" => "",
        "Email sent?" => "No",
        "Redeemed" => "No",
        "Tracking " => nil,
        "Sent by" => nil,
        "Sent On" => nil,
        "Duplicate?" => nil,
        "Notes" => nil,
        :row_index => 4 }]
  end

  before do
    allow(Helper::GoogleSheets).to receive(:new).and_return(helper)
    allow(helper).to receive(:read).and_return(test_sheet)
  end

  describe '#sheet_contents' do
    it 'returns the sheet contents' do
      expect(instance.sheet_contents).to eq(test_sheet)
    end
  end

  describe '#find_code_row' do
    it 'returns a row with an available code' do
      result = instance.find_code_row(25)

      expect(result['Code']).to eq('AVAILABLE_CODE')
      expect(result['Email sent?']).not_to eq('Yes')
    end

    it 'raises a NoCodeAvailableError if no code is available' do
      expect { instance.find_code_row(50) }.to raise_error(RewardProvider::Boundless::NoCodeAvailableError)
    end

    it 'raises an InvalidCodeValueError if the code value is invalid' do
      expect { instance.find_code_row(100) }.to raise_error(RewardProvider::Boundless::InvalidCodeValueError)
    end
  end

  describe '#save_row_values' do
    before do
      allow(helper).to receive_messages(write_values: 2)
    end

    it 'updates the sheet with the new row' do
      result = instance.save_row_values({ 'Code' => 'UPDATE_CODE', 'Email sent?' => 'Yes', :row_index => 4 })

      expect(result).to eq(2)
      expect(helper).to have_received(:write_values).with(4, { "Code" => "UPDATE_CODE", "Email sent?" => "Yes" })
    end
  end

  describe '#claim' do
    before do
      allow(helper).to receive_messages(write_values: 5)
    end

    it 'claims a code' do
      result = instance.claim(25, "Claimant", "First MR Merged")

      expect(result).to eq('AVAILABLE_CODE')
      expect(helper).to have_received(:write_values).with(
        4,
        {
          "Email Recipient" => "Claimant",
          "Email sent?" => "Yes",
          "Prize" => "First MR Merged",
          "Sent On" => Date.today.to_s,
          "Sent by" => "Rewards Automaton"
        }
      )
    end
  end
end
