# frozen_string_literal: true

require_relative '../../lib/reward_provider/base'

RSpec.describe RewardProvider::Base do
  describe '.claim' do
    it 'raises a NotImplementedError' do
      expect { described_class.new.claim(1, 2, 3) }.to raise_error(NotImplementedError)
    end
  end
end
