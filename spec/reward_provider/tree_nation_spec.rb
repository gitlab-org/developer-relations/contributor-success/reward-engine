# frozen_string_literal: true

require_relative '../../lib/reward_provider/tree_nation'

RSpec.describe RewardProvider::TreeNation do
  let(:recipient_name) { 'Lee Tickett' }
  let(:message) { 'Thank you for contributing!' }

  before do
    stub_env('TREE_NATION_API_TOKEN', 'tree-nation-api-token')
  end

  describe '#projects', :vcr do
    subject(:projects) { described_class.new.projects }

    it 'returns active projects with trees starting from 1EUR or less' do
      expect(projects).to contain_exactly(5, 6, 7, 11, 21, 41, 132, 163, 200, 213, 281, 295, 328, 362, 368, 371)
    end
  end

  describe '#species', :vcr do
    subject(:species) { described_class.new.species(5) }

    it 'returns species with stock priced at 1EUR or less' do
      expect(species).to contain_exactly(
        { id: 7, price: 1 },
        { id: 8, price: 1 },
        { id: 9, price: 1 }
      )
    end
  end

  describe '#plant', :vcr do
    subject(:plant) { described_class.new.plant(recipient_name, 8, 10, message) }

    it 'calls the expected API endpoint with the expected payload and returns a token' do
      expect(plant).to eq('655b8375c89e5')
    end
  end

  describe '#claim' do
    let(:instance) { described_class.new }

    before do
      allow(instance).to receive(:projects).and_return([1])
      allow(instance).to receive(:species).with(1).and_return([{ id: 2, price: 0.5 }])
      allow(instance).to receive(:plant).with(recipient_name, 2, 50, message).and_return('abc123')
    end

    it 'grabs proejcts, species and plants trees' do
      expect(instance.claim(25, recipient_name, message)).to eq('abc123')

      expect(instance).to have_received(:projects)
      expect(instance).to have_received(:species).with(1)
      expect(instance).to have_received(:plant).with(recipient_name, 2, 50, message)
    end
  end
end
