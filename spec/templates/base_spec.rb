# frozen_string_literal: true

require_relative '../../lib/templates/base'

RSpec.describe Templates::Base do
  describe '.build' do
    it 'raises a NotImplementedError' do
      expect { described_class.new.build(1) }.to raise_error(NotImplementedError)
    end
  end

  describe '.validate_args!' do
    it 'raises a NotImplementedError' do
      expect { described_class.new.validate_args!(1) }.to raise_error(NotImplementedError)
    end
  end
end
