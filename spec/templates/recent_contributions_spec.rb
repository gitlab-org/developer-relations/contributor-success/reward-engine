# frozen_string_literal: true

require_relative '../../lib/templates/recent_contributions'

RSpec.describe Templates::RecentContributions do
  let(:args) { { username: 'USERNAME', due_date: '2050-01-30' } }

  describe '.build' do
    subject(:message) { described_class.new.build(args) }

    it 'includes a personalized welcome' do
      expect(message).to start_with('Hello @USERNAME :wave:')
    end

    it 'includes the expected quick actions' do
      quick_actions = <<~EOQA
        /assign @USERNAME
        /confidential
        /label ~reward::tree
        /due 2050-01-30
      EOQA
      expect(message).to end_with(quick_actions)
    end

    it 'does not include `see more`' do
      expect(message).not_to include('See more')
    end

    it 'does not include `/cc`' do
      expect(message).not_to include('/cc')
    end

    context 'with a link' do
      let(:args) { { link: 'LINK', username: 'USERNAME' } }

      it 'includes `see more` with the link' do
        expect(message).to include('See more: LINK')
      end
    end

    context 'with a single cc' do
      let(:args) { { link: 'LINK', username: 'USERNAME', cc: 'lee' } }

      it 'includes `see more` with the link' do
        expect(message).to include('/cc @lee')
      end
    end

    context 'with multiple ccs' do
      let(:args) { { link: 'LINK', username: 'USERNAME', cc: %w[lee oksana] } }

      it 'includes `see more` with the link' do
        expect(message).to include('/cc @lee @oksana')
      end
    end
  end

  describe '.validate_args!' do
    subject(:validate_args!) { described_class.new.validate_args!(args) }

    it 'does not raise an error' do
      expect { validate_args! }.not_to raise_error
    end

    context 'with no due_date' do
      let(:args) { { username: 'USERNAME' } }

      it 'raises an error' do
        expect { validate_args! }.to raise_error(ArgumentError, 'Due date cannot be nil')
      end
    end
  end
end
