# frozen_string_literal: true

require 'pry-byebug'
require 'simplecov-cobertura'
require 'stub_env'
require 'timecop'
require 'vcr'
require 'webmock/rspec'

Dir[File.expand_path('support/**/*.rb', __dir__)].each { |f| require f }

RSpec.configure do |config|
  config.include StubEnv::Helpers

  config.before(:suite) do
    Timecop.safe_mode = true
    WebMock.disable_net_connect!
  end

  original_stderr = $stderr
  original_stdout = $stdout

  config.before(:all) do
    $stderr = File.open(File::NULL, 'w')
    $stdout = File.open(File::NULL, 'w')
  end

  config.after(:all) do
    $stderr = original_stderr
    $stdout = original_stdout
  end

  config.expect_with :rspec do |r|
    r.max_formatted_output_length = nil
  end
end

SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new([SimpleCov::Formatter::CoberturaFormatter])
SimpleCov.start do
  load_profile 'rails'
  add_filter 'vendor'
  add_group 'Executables', 'bin'
end

VCR.configure do |config|
  config.cassette_library_dir = 'spec/cassettes'
  config.hook_into :webmock
  config.default_cassette_options = { match_requests_on: %i[method uri headers body] }
  config.configure_rspec_metadata!
end
