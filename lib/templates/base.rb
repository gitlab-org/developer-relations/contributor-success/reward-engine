# frozen_string_literal: true

module Templates
  class Base
    def build(args)
      raise NotImplementedError
    end

    def validate_args!(args)
      raise NotImplementedError
    end
  end
end
