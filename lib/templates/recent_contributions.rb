# frozen_string_literal: true

module Templates
  class RecentContributions
    def build(args)
      see_more = "\nSee more: #{args[:link]}\n" unless args[:link].nil?
      args[:cc] = Array(args[:cc])
      cc = "/cc @#{args[:cc].join(' @')}" unless args[:cc].empty?

      <<~EOMSG
        Hello @#{args[:username]} :wave:

        We are reaching out to thank you for your recent contributions.
        We appreciate everyone's effort to contribute to GitLab and we are so excited to have such a wonderful community.
        #{see_more}
        As a commitment to sustainability, we now offer to plant trees as an alternative to sending swag.
        Please comment below to let us know if you would prefer swag or trees.

        In 1-month's time, if we have not heard from you, we will close this issue, plant trees and send you the redemption details.

        Thanks again for your contributions to GitLab!#{' '}

        Sincerely,

        The GitLab Developer Relations team

        Tip: Did you know there are amazing resources available for contributing to GitLab over at https://about.gitlab.com/community/contribute/

        Due to applicable trade control law and the policies of our shipping partners, prizes cannot be shipped to the following countries: Cuba, Iran, North Korea, Syria, Ukraine, Russia, and Belarus.

        #{cc}
        /assign @#{args[:username]}
        /confidential
        /label ~reward::tree
        /due #{args[:due_date]}
      EOMSG
    end

    def validate_args!(args)
      raise ArgumentError, 'Due date cannot be nil' if args[:due_date].nil?
    end
  end
end
