# frozen_string_literal: true

module Helper
  class MessageTemplate
    def initialize(reward_issuer, template)
      @reward_issuer = reward_issuer
      @template = template
    end

    def build(args)
      validate_args!(args)

      @template.build(args)
    end

    def validate_args!(args)
      raise ArgumentError, 'Message cannot be nil' unless args.is_a?(Hash)

      @reward_issuer.validate_args!(args)
      @template.validate_args!(args)
    end
  end
end
