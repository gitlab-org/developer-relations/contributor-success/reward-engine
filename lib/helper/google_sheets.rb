# frozen_string_literal: true

require 'google/apis/sheets_v4'
require 'googleauth'

module Helper
  # Interface for Google Sheets (wrapping the Google sheet API)
  class GoogleSheets
    def initialize(sheet_id, sheet_name)
      google_service_account_credentials_path = ENV.fetch('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH')
      @service = Google::Apis::SheetsV4::SheetsService.new
      @service.authorization = Google::Auth::ServiceAccountCredentials.make_creds(
        json_key_io: File.open(google_service_account_credentials_path),
        scope: 'https://www.googleapis.com/auth/drive'
      )
      @sheet_name = sheet_name
      @sheet_id = sheet_id
      @last_read = fresh_read
    end

    def read
      @last_read ||= fresh_read
    end

    def fresh_read
      rows = @service.get_spreadsheet_values(@sheet_id, @sheet_name).values
      headers = rows.shift
      map_headers(headers)

      # the header is row 1, so we start at row 2
      row_index = 2

      rows.map do |row|
        new_row = headers.zip(row).to_h
        new_row[:row_index] = row_index
        row_index += 1

        new_row
      end
    end

    def calculate_sheet_range(row)
      row_index = row[:row_index]
      col_max = letter_lookup[row.keys.count - 1]

      "A#{row_index}:#{col_max}#{row_index}"
    end

    def get_header_column(header_field)
      @header_map[header_field]
    end

    def write(range, value)
      value = Array(value)

      value_range = Google::Apis::SheetsV4::ValueRange.new(values: [value])
      update_range = "#{@sheet_name}!#{range}"
      update_response = @service.update_spreadsheet_value(
        @sheet_id,
        update_range,
        value_range,
        value_input_option: 'USER_ENTERED'
      )

      update_response.updated_cells
    end

    def write_values(row_num, values)
      all_values = values.map do |cell, value|
        column = get_header_column(cell)

        Google::Apis::SheetsV4::ValueRange.new(
          range: "#{@sheet_name}!#{column}#{row_num}",
          values: [[value]]
        )
      end

      batch_update_request = Google::Apis::SheetsV4::BatchUpdateValuesRequest.new(
        data: all_values,
        value_input_option: 'USER_ENTERED'
      )

      @service.batch_update_values(@sheet_id, batch_update_request).total_updated_cells
    end

    private

    def letter_lookup
      @letter_lookup ||= (1..26).zip('A'..'Z').to_h
    end

    def map_headers(headers)
      @header_map ||= headers.map.with_index do |header, index|
        { header => letter_lookup[index + 1] }
      end.reduce({}, :merge)
    end
  end
end
