# frozen_string_literal: true

require 'httparty'

require_relative 'base'

module RewardProvider
  # Tree nation tree provider
  class TreeNation < Base
    TREE_NATION_PROD_API = 'https://tree-nation.com/api'
    TREE_NATION_TEST_API = 'https://youcannevertestenough.tree-nation.com/api'

    def initialize
      dry_run = ENV.fetch('DRY_RUN', '1')
      @base_url = dry_run == '0' ? TREE_NATION_PROD_API : TREE_NATION_TEST_API
      @api_token = ENV.fetch('TREE_NATION_API_TOKEN')

      super
    end

    def claim(value, recipient_name, message)
      project_id = projects.sample
      species = species(project_id).sample
      quantity = (value / species[:price]).floor

      plant(recipient_name, species[:id], quantity, message)
    end

    def projects
      records = get_json('/projects?status=active')
      records.select { |record| record['species_price_from'].to_f <= 1 }.map { |record| record['id'] }
    end

    def species(project_id)
      all_species = get_json("/projects/#{project_id}/species")
      available_species = all_species.select do |record|
        record['stock'].to_i.positive? && record['price'].to_f <= 1
      end
      available_species.map do |record|
        {
          id: record['id'],
          price: record['price']
        }
      end
    end

    def plant(recipient_name, species_id, quantity, message)
      response = HTTParty.post(
        "#{@base_url}/plant",
        headers: headers,
        body: {
          recipients: { name: recipient_name },
          species_id: species_id,
          quantity: quantity,
          message: message
        }.to_json
      )

      JSON.parse(response.body)['trees'].first['token']
    end

    def headers
      { 'Authorization' => "Bearer #{@api_token}" }
    end

    def get_json(path)
      response = HTTParty.get(
        "#{@base_url}#{path}",
        headers: headers
      )
      JSON.parse(response.body)
    end
  end
end
