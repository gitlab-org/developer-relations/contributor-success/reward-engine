# frozen_string_literal: true

module RewardProvider
  # Interface for reward providers (e.g. those providing the rewards)
  class Base
    def claim(value, recipient_name, message)
      raise NotImplementedError
    end
  end
end
