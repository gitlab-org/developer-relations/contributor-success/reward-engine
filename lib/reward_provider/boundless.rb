# frozen_string_literal: true

require_relative '../helper/google_sheets'
require_relative 'base'

module RewardProvider
  # Boundless swag provider
  class Boundless < Base
    BOUNDLESS_SHEET_NAME = 'One-time Coupon codes'

    BOUNDLESS_VALID_CODE_VALUES = [10, 25, 50, 75].freeze

    BOUNDLESS_HEADER_FIELD_CODE = 'Code'
    BOUNDLESS_HEADER_FIELD_PRIZE = 'Prize'
    BOUNDLESS_HEADER_FIELD_RECIPIENT = 'Email Recipient'
    BOUNDLESS_HEADER_FIELD_SENDER = 'Sent by'
    BOUNDLESS_HEADER_FIELD_SEND_DATE = 'Sent On'
    BOUNDLESS_HEADER_FIELD_SENT = 'Email sent?'
    BOUNDLESS_HEADER_FIELD_VALUE = 'Items or value'

    InvalidCodeValueError = Class.new(StandardError)
    NoCodeAvailableError = Class.new(StandardError)

    def initialize
      @sheet_id = ENV.fetch('BOUNDLESS_SHEET_ID', nil)
      @sheet = Helper::GoogleSheets.new(@sheet_id, BOUNDLESS_SHEET_NAME)

      super
    end

    def claim(value, recipient_name, message)
      claim_row = find_code_row(value)

      new_row_values = {}

      new_row_values[:row_index] = claim_row[:row_index]
      new_row_values[BOUNDLESS_HEADER_FIELD_PRIZE] = message
      new_row_values[BOUNDLESS_HEADER_FIELD_SENT] = 'Yes'
      new_row_values[BOUNDLESS_HEADER_FIELD_RECIPIENT] = recipient_name
      new_row_values[BOUNDLESS_HEADER_FIELD_SENDER] = 'Rewards Automaton'
      new_row_values[BOUNDLESS_HEADER_FIELD_SEND_DATE] = Date.today.to_s

      save_row_values(new_row_values)

      claim_row[BOUNDLESS_HEADER_FIELD_CODE]
    end

    def save_row_values(row)
      row_index = row.delete(:row_index)

      @sheet.write_values(row_index, row)
    end

    def find_code_row(value)
      raise InvalidCodeValueError, "Invalid code value #{value}" unless BOUNDLESS_VALID_CODE_VALUES.include?(value)

      rows_of_value = sheet_contents.select do |row|
        row[BOUNDLESS_HEADER_FIELD_SENT] != 'Yes' && row[BOUNDLESS_HEADER_FIELD_VALUE].delete('$').to_i == value
      end

      raise NoCodeAvailableError, "No code found for value #{value}" if rows_of_value.empty?

      rows_of_value.first
    end

    def sheet_contents
      @sheet.read
    end
  end
end
