# frozen_string_literal: true

module RewardIssuer
  # Create the RewardIssuer class
  class Base
    def issue(args)
      raise NotImplementedError
    end

    def validate_args!(args)
      raise NotImplementedError
    end
  end
end
