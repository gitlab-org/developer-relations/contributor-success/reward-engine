# frozen_string_literal: true

require 'gitlab'

require_relative 'base'

module RewardIssuer
  # Create the GitLabIssuer class
  class Gitlab < Base
    # https://gitlab.com/gitlab-org/developer-relations/contributor-success/rewards
    REWARD_PROJECT_ID = 48_412_664

    def initialize
      @client = ::Gitlab.client(
        endpoint: ENV.fetch('GITLAB_ENDPOINT'),
        private_token: ENV.fetch('GITLAB_TOKEN')
      )
    end

    def issue(args); end

    def validate_args!(args)
      raise ArgumentError, 'Username cannot be nil' if args[:username].nil?
    end
  end
end
